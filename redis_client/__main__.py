import sys

from redis_client.app.main import main as app_main


def main():
    sys.exit(app_main())


if __name__ == "__main__":
    main()

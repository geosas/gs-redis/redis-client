import logging

import typer

from redis_client.connection.connection import ConnectionBase, TcpConnection


def request_should_exit(request: bytes) -> bool:
    return request in {b'q', b'x', b'exit', b'quit'}


def string_unescape(string: str) -> str:
    return string.encode('utf8').decode('unicode_escape')


def request_encode(command: str) -> bytes:
    request_by_command: dict[str, str] = {
        'ping': '*1\r\n$4\r\nPING\r\n',
    }
    return request_by_command.get(
        command,
        string_unescape(command),
    ).encode('utf8')


def request_read(prompt: str) -> bytes:
    command = input(prompt)
    return request_encode(command)


def communicate(connection: ConnectionBase):
    while True:
        request = request_read('Enter request: ')
        if request_should_exit(request):
            logging.debug('Exit pattern entered. Exiting.')
            break
        response = connection.request(request)
        print(response)


def cli(
    host: str = '127.0.0.1',
    port: int = 6379,
):
    logging.basicConfig(level=logging.DEBUG)
    with TcpConnection(host=host, port=port).connect() as connection:
        communicate(connection)


def main():
    typer.run(cli)

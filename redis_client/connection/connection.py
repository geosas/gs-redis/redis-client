import socket
from abc import ABC, abstractmethod
from typing import Self

from redis_client.config import MAX_MESSAGE_BYTES


class ConnectionBase(ABC):
    def __enter__(self):
        return self.connect()

    def __exit__(self, exc_type, exc_value, traceback):
        if not exc_type:
            self.disconnect()

    @abstractmethod
    def connect(self) -> Self:
        """Attempt to create a connection."""

    @abstractmethod
    def disconnect(self):
        """Terminate the connection and free up connection resources."""

    @abstractmethod
    def request(self, message: bytes) -> bytes:
        """Send a request and return response."""


class TcpConnection(ConnectionBase):
    def __init__(
        self,
        host: str,
        port: int,
    ):
        self.host = host
        self.port = port

    def connect(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.host, self.port))
        return self

    def disconnect(self):
        self.socket.close()

    def request(self, message: bytes, length: int = MAX_MESSAGE_BYTES) -> bytes:
        self.socket.send(message)
        return self.socket.recv(length)


c = TcpConnection(host='', port=1)

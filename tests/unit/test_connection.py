import pytest

from redis_client.connection.connection import ConnectionBase


class TestConnect:
    def test(self, test_connection: ConnectionBase):
        with test_connection.connect():
            pass


class TestPing:
    @pytest.mark.parametrize(
        (
            'request_',
            'expected_response',
        ),
        [
            (
                b'*1\r\n$4\r\nPING\r\n',
                b'+PONG\r\n',
            ),
        ],
    )
    def test(
        self,
        test_connection: ConnectionBase,
        request_: bytes,
        expected_response: bytes,
    ):
        with test_connection.connect() as connection:
            response = connection.request(request_)
        assert response == expected_response

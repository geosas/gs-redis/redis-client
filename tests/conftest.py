from typing import ClassVar

import pytest

from redis_client.connection.connection import ConnectionBase


class TestConnection(ConnectionBase):
    unknown_message_response = b'Unknown message'
    response_by_message: ClassVar = {
        b'*1\r\n$4\r\nPING\r\n': b'+PONG\r\n',
    }

    def __init__(self):
        pass

    def connect(self):
        return self

    def disconnect(self):
        pass

    def request(self, message):
        return self.response_by_message.get(
            message,
            self.unknown_message_response,
        )


@pytest.fixture()
def test_connection():
    return TestConnection()
